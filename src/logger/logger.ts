import * as Rollbar from 'rollbar';
import {
  Logger as NestLogger,
  LoggerService,
  Injectable,
} from '@nestjs/common';

@Injectable()
export class Logger implements LoggerService {
  private readonly rollbar: Rollbar;
  private readonly nestLogger: NestLogger;
  private readonly microservice = 'post';

  constructor(context?: string) {
    this.rollbar =
      process.env.NODE_ENV !== 'test'
        ? new Rollbar({
            accessToken: '8789c04c1e874d63915e0532aba41152',
            captureUncaught: true,
            captureUnhandledRejections: true,
            environment: process.env.NODE_ENV,
            logLevel: 'info',
            payload: {
              server: {
                branch: 'master',
                root: 'E:\\Projects\\Ipet\\post-microservice\\', // temporary for development
              },
            },
          })
        : null;

    this.nestLogger = new NestLogger(context);
  }
  log(message: any, context?: string) {
    this.nestLogger.log(message, context);
  }
  warn(message: any, context?: string) {
    this.nestLogger.warn(message, context);
  }
  debug?(message: any, context?: string) {
    this.nestLogger.debug(message, context);
  }
  verbose?(message: any, context?: string) {
    this.nestLogger.verbose(message, context);
  }

  info(message: string, context?: string, payload?: {}): void {
    this.rollbar?.info(message, {
      microservice: this.microservice,
      context,
      ...payload,
    });

    this.nestLogger.log(message, context);
  }

  error(
    message: string,
    trace?: string,
    context?: string,
    error?: Error,
    payload?: {},
  ): void {
    this.rollbar?.error(message, error, {
      microservice: this.microservice,
      context,
      ...payload,
    });

    this.nestLogger.error(message, error.stack, context);
  }
}
