import { NestFactory } from '@nestjs/core';
import { MicroserviceModule } from './microservice.module';
import { Logger } from './logger/logger';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

const logger = new Logger('Main');

async function bootstrap() {
  const app = await NestFactory.create(MicroserviceModule);

  const options = new DocumentBuilder()
    .setTitle('Ipet - Post Microservice')
    .setDescription('Post Microservice')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup('api', app, document);

  const port = process.env.PORT || 8820;

  app.listen(port, () => {
    logger.info(`Post Microservice is listening on  port ${port}`);
  });
}

bootstrap();
