import { Module } from '@nestjs/common';
import { PostModule } from './post/post.module';
import { MongooseModule } from '@nestjs/mongoose';
import { getMongoURI } from './config/mongo';

@Module({
  imports: [
    PostModule,
    MongooseModule.forRoot(getMongoURI()),
  ],
  controllers: [],
  providers: [],
})
export class MicroserviceModule {}
