import * as mongoose from 'mongoose';
export const PostSchema = new mongoose.Schema({
  userId: String,
  description: String,
  fileId: String,
  date: Date
});
