import { mock, instance, when, verify, anything } from 'ts-mockito';
import { PostService } from './post.service';
import { Model, Document } from 'mongoose';
import { Post } from './post.interface';

describe('PostService.unit', () => {
  const mockedModel: Model<Post & Document> = mock(Model);
  let postService: PostService;
  let post: Post;

  beforeAll(() => {
    postService = new PostService(instance(mockedModel));
    post = {
      userId: 'userIdTest',
      description: 'descriptionTest',
      fileId: 'fileIdTest',
      date: new Date(),
    };
  });

  describe('create()', () => {
    it('should creat post', async () => {
      postService.create(post);
    });
  });
});
