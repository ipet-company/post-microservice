import { Module } from '@nestjs/common';
import { PostService } from './post.service';
import { PostController } from './post.controller';
import { Logger } from '../logger/logger';
import { MongooseModule } from '@nestjs/mongoose';
import { PostSchema } from './post.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'post', schema: PostSchema }]),
  ],
  controllers: [PostController],
  providers: [PostService, Logger, String],
})
export class PostModule {}
