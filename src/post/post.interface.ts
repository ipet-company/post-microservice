export interface Post {
  userId: string;
  description: string;
  fileId: string;
  date: Date;
}