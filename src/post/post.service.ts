import { Injectable } from '@nestjs/common';
import { Post } from './post.interface';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Document } from 'mongoose';

@Injectable()
export class PostService {
  constructor(
    @InjectModel('post')
    private readonly postModel: Model<Post & Document>,
  ) {}

  async get(userId: string = null): Promise<Post[]> {
    let unsortedPosts: Post[];
    if (userId) {
      unsortedPosts = await this.postModel.find({ userId: userId }).exec();
    } else {
      unsortedPosts = await this.postModel.find().exec();
    }

    return unsortedPosts.sort(this.sortByDate);
  }

  async create(post: Post): Promise<Post> {
    return await this.postModel.create(post);
  }

  async update(id: any, post: Post): Promise<void> {
    await this.postModel.updateOne({ _id: id }, post).exec();
  }

  async remove(id: any): Promise<void> {
    const result = await this.postModel.deleteOne({ _id: id }).exec();
  }

  private readonly sortByDate = (a: Post, b: Post) =>
    a.date > b.date ? -1 : a.date < b.date ? 1 : 0;
}
