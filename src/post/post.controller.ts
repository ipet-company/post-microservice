import {
  Controller,
  Get,
  Post as HttpPost,
  Put,
  Delete,
  Body,
  Param,
} from '@nestjs/common';
import { PostService } from './post.service';
import { Logger } from '../logger/logger';
import { Post } from './post.interface';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('posts')
@Controller('posts')
export class PostController {
  constructor(
    private readonly postService: PostService,
    private readonly logger: Logger,
  ) { }

  @Get('by-user/:id')
  async getUserPosts(userId: string): Promise<Post[]> {
    this.logger.info(
      'GetUserPosts request received',
      `${PostController.name}.${this.getUserPosts.name}`,
    );

    return this.postService.get(userId);
  }

  @Get()
  async getAllPosts(): Promise<Post[]> {
    this.logger.info(
      'Get All Posts request received',
      `${PostController.name}.${this.getAllPosts.name}`,
    );

    return this.postService.get();
  }

  @HttpPost()
  async createUserPost(@Body() post: Post): Promise<Post> {
    this.logger.info(
      'Create Post request received',
      `${PostController.name}.${this.createUserPost.name}`,
    );

    return this.postService.create(post);
  }

  @Put(':postId')
  async updateUserPost(@Param('postId') postId: string, @Body() post: Post): Promise<void> {
    this.logger.info(
      'Update User Post request received',
      `${PostController.name}.${this.updateUserPost.name}`,
    );

    return this.postService.update(postId, post);
  }

  @Delete(':postId')
  async deleteUserPost(@Param('postId') postId: string): Promise<void> {
    this.logger.info(
      'Delete User Post request received',
      `${PostController.name}.${this.deleteUserPost.name}`,
    );

    return this.postService.remove(postId);
  }
}
